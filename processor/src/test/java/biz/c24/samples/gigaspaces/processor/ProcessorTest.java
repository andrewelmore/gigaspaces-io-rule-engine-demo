package biz.c24.samples.gigaspaces.processor;

import biz.c24.samples.gigaspaces.common.Message;
import biz.c24.samples.gigaspaces.common.Message.State;

import org.junit.Test;
import static org.junit.Assert.assertEquals;


/**
 * A simple unit test that verifies the Processor processData method actually processes
 * the Data object.
 */
public class ProcessorTest {

    @Test
    public void verifyParseProcessor() {
        ParseProcessor processor = new ParseProcessor();
        Message data = new Message("invalid");

        Message result = processor.processData(data);
        assertEquals("verify that the data object was processed", true, (result.getState() == State.VALID) || (result.getState() == State.INVALID));
    }
}
