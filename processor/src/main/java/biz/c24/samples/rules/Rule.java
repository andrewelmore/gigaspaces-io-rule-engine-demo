package biz.c24.samples.rules;

import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;

import biz.c24.io.api.data.ComplexDataObject;

@SpaceClass
public abstract class Rule<T> {

	private Logger log =  LoggerFactory.getLogger(Rule.class);

	private String name;
	private T value;
	private Collection<Rule<?>> upstreamRules = new LinkedList<Rule<?>>();


	public Rule(String name) {
		this.name = name;
	}
	
	protected abstract boolean calculate();
	protected abstract boolean calculate(ComplexDataObject obj);
	
	public final void recalculate(ComplexDataObject obj) {
		if(calculate(obj)) {
			for(Rule<?> rule : upstreamRules) {
				rule.recalculate();
			}
		}
	}
	
	public final void recalculate() {
		if(calculate()) {
			for(Rule<?> rule : upstreamRules) {
				rule.recalculate();
			}
		}
	}
	
	public T getValue() {
		return value;
	}
	
	protected void setValue(T newValue) {
		this.value = newValue;
	}
	
    @SpaceId
    public String getName() {
    	return name;
    }
    
    public void addDependent(Rule rule) {
    	upstreamRules.add(rule);
    }
	
}
