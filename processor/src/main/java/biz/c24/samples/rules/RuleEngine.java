package biz.c24.samples.rules;

import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.c24.io.api.data.ComplexDataObject;

public class RuleEngine {
	
	private Logger log = LoggerFactory.getLogger(RuleEngine.class);
	
	private Collection<Rule<?>> rules = new LinkedList<Rule<?>>();
	
	public RuleEngine(Collection<Rule<?>> rules) {
		this.rules = rules;
	}
	
	public void addRule(Rule<?> rule) {
		rules.add(rule);
	}
	
	public void process(ComplexDataObject obj) {
		for(Rule<?> rule : rules) {
			rule.recalculate(obj);
		}
	}

}
