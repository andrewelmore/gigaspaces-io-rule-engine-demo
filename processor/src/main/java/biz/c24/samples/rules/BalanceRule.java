package biz.c24.samples.rules;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.c24.io.api.data.ComplexDataObject;
import biz.c24.io.swift2012.MT103Message;

public class BalanceRule extends Rule<BigDecimal>{
	
	private static Logger log = LoggerFactory.getLogger(BalanceRule.class);

	private String bank;
	
	public BalanceRule(String bank) {
		super("Net balance for " + bank);
		this.bank = bank;
		setValue(new BigDecimal(0));
	}

	@Override
	public boolean calculate() {
		// We only change when we receive a corresponding object as we have no dependencies
		return false;
	}

	@Override
	public boolean calculate(ComplexDataObject obj) {
		boolean processed = false;

		if(obj instanceof MT103Message) {
			MT103Message msg = (MT103Message)obj;
			
			// Now see if the message affects us
			if(bank.equals(msg.getBlock4().getField52aOrderingInstitution().getA().getBIC().getBankCode())) {
				setValue(getValue().subtract(msg.getBlock4().getField32aDatesAndAmounts().getA().getCurrencyAmount().getAmount()));
				log.info("Balance for " + bank + " decreased to " + getValue());
				processed = true;
			}
			if(bank.equals(msg.getBlock4().getField53aSendersCorrespondent().getA().getBIC().getBankCode())) {
				setValue(getValue().add(msg.getBlock4().getField32aDatesAndAmounts().getA().getCurrencyAmount().getAmount()));
				log.info("Balance for " + bank + " increased to " + getValue());
				processed = true;
			}
		}

		return processed;
	}

}
