package biz.c24.samples.rules;

import java.math.BigDecimal;

import biz.c24.io.api.data.ComplexDataObject;

public class SumRule extends Rule<BigDecimal> {
	private Rule<BigDecimal>[] subRules;
	
	public SumRule(String name, Rule<BigDecimal>... subRules) {
		super(name);
		this.subRules = subRules;
		
		for(Rule rule : subRules) {
			rule.addDependent(this);
		}
	}

	@Override
	protected boolean calculate() {
		// See if things have changed
		BigDecimal old = getValue();
		BigDecimal curr = new BigDecimal(0);
		for(Rule<BigDecimal> rule : subRules) {
			curr = curr.add(rule.getValue());
		}
		if(curr.equals(old)) {
			return false;
		} else {
			setValue(curr);
			return true;
		}
		
		
	}

	@Override
	protected boolean calculate(ComplexDataObject obj) {
		// We're purely an aggregator
		return false;
	}
	
	

}
