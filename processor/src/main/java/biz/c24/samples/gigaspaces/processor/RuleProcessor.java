package biz.c24.samples.gigaspaces.processor;

import biz.c24.io.api.data.ComplexDataObject;
import biz.c24.samples.gigaspaces.common.Message;
import biz.c24.samples.gigaspaces.common.Message.State;
import biz.c24.samples.rules.RuleEngine;

import org.openspaces.events.EventDriven;
import org.openspaces.events.EventTemplate;
import org.openspaces.events.adapter.SpaceDataEvent;
import org.openspaces.events.notify.Notify;
import org.openspaces.events.notify.NotifyType;
import java.util.logging.Logger;

/**
 * The processor simulates work done no un-processed Data object. The processData
 * accepts a Data object, simulate work by sleeping, and then sets the processed
 * flag to true and returns the processed Data.
 */
@EventDriven @Notify
@NotifyType(write = true, update = true)
public class RuleProcessor<T extends ComplexDataObject> {

    Logger log= Logger.getLogger(this.getClass().getName());

    private RuleEngine engine;
    
    public RuleProcessor(RuleEngine engine) {
    	this.engine = engine;
    }
   
    
    /**
     * We process any messages in a valid state
     * @return
     */
    @EventTemplate
    Message<T> templateMessage() {
    	Message<T> msg = new Message<T>();
    	msg.setState(Message.State.VALID);
    	return msg;
    }

    /**
     * Process the given Data object and returning the processed Data.
     *
     * Can be invoked using OpenSpaces Events when a matching event
     * occurs.
     */
    @SpaceDataEvent
    public Message<T> processData(Message<T> data) {
    	

    		log.info("Processing " + data.getId());
    		engine.process(data.getMessage());
    		data.setState(State.PROCESSED);

        return data;
    }

}
