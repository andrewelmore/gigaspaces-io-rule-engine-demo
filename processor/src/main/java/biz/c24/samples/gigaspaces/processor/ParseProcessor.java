package biz.c24.samples.gigaspaces.processor;

import biz.c24.io.api.C24;
import biz.c24.io.api.data.ComplexDataObject;
import biz.c24.io.swift2012.MT103Message;
import biz.c24.samples.gigaspaces.common.Message;
import biz.c24.samples.gigaspaces.common.Message.State;

import org.openspaces.events.EventDriven;
import org.openspaces.events.EventTemplate;
import org.openspaces.events.adapter.SpaceDataEvent;
import org.openspaces.events.polling.Polling;

import java.io.StringReader;
import java.util.logging.Logger;

/**
 * The processor simulates work done no un-processed Data object. The processData
 * accepts a Data object, simulate work by sleeping, and then sets the processed
 * flag to true and returns the processed Data.
 */
@EventDriven @Polling(concurrentConsumers = 8, maxConcurrentConsumers = 8)
public class ParseProcessor<T extends ComplexDataObject> {

    Logger log = Logger.getLogger(this.getClass().getName());
    
    /**
     * We process any messages in a new state
     * @return
     */
    @EventTemplate
    Message<T> templateMessage() {
    	Message<T> msg = new Message<T>();
    	msg.setState(Message.State.NEW);
    	return msg;
    }

    /**
     * Process the given Data object and returning the processed Data.
     *
     * Can be invoked using OpenSpaces Events when a matching event
     * occurs.
     */
    @SuppressWarnings("unchecked")
	@SpaceDataEvent
    public Message<T> processData(Message<T> data) {
    	
    	try {
    		log.info("Parsing " + data.getId());
    		
    		data.setMessage((T)C24.parse(MT103Message.class, new StringReader(data.getRawMessage())));
    		data.setState(State.VALID);
    		
    		log.info("Parsed " + data.getId());
    	} catch(Throwable ex) {
    		log.info("Message invalid " + data.getId());
    		data.setFailure(ex);
    		data.setState(State.INVALID);
    	}
    	
        return data;
    }

}
