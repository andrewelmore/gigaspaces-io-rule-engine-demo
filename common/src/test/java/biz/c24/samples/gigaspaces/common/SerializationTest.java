package biz.c24.samples.gigaspaces.common;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;

import biz.c24.io.api.C24;
import biz.c24.io.api.ParserException;
import biz.c24.io.api.data.ComplexDataObject;
import biz.c24.io.swift2012.MT103Message;
import biz.c24.samples.gigaspaces.common.Message.State;

import org.junit.Test;

public class SerializationTest {
	
	@Test
	public void testSerialization() throws IOException {
		Message<ComplexDataObject> msg = new Message<ComplexDataObject>();
		ObjectOutputStream oos = new ObjectOutputStream(new ByteArrayOutputStream());
		oos.writeObject(msg);
		oos.close();
	}
	
	@Test
	public void testInvalidMessage() throws IOException {
		
		Message<MT103Message> obj = new Message<MT103Message>();
		try {
			obj.setMessage((MT103Message) C24.parse(MT103Message.class, new InputStreamReader(getClass().getResourceAsStream("/invalid.dat"))));
			fail("Parsing unexpectedly succeeded");
		} catch(ParserException ex) {
			obj.setState(State.INVALID);
			obj.setFailure(ex);
		}
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(obj);
		oos.close();
		//assertThat(bos.toByteArray().length, is(lessThan(some size)));

	
		
	}

}
