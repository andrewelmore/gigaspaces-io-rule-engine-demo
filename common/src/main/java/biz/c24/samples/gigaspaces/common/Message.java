package biz.c24.samples.gigaspaces.common;

import java.io.Serializable;

import biz.c24.io.api.data.ComplexDataObject;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceRouting;


/**
 * A simple object used to work with the Space. Important properties include the id
 * of the object, a type (used to perform routing when working with partitioned space),
 * the raw data and processed data, and a boolean flag indicating if this Data object
 * was processed or not.
 */
@SpaceClass
public class Message<T extends ComplexDataObject> implements Serializable {

	public static enum State {
		NEW,
		VALID,
		PROCESSED,
		INVALID
	};

	
    private String id = null;

    private State state = null;

    private String rawMessage = null;
    
    private T message = null;
    
    private Throwable failure = null;


    /**
     * Constructs a new Data object.
     */
    public Message() {

    }

    /**
     * Constructs a new Data object with the given type
     * and raw data.
     */
    public Message(String rawMessage) {
        this.rawMessage = rawMessage;
        this.state = State.NEW;
    }

    /**
     * The id of this object.
     */
    @SpaceId(autoGenerate=true)
    public String getId() {
        return id;
    }

    /**
     * The id of this object. Its value will be auto generated when it is written
     * to the space.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * The current state of the data object. Used as the routing field when working with
     * a partitioned space.
     */
    // @SpaceRouting
    public State getState() {
        return state;
    }

    /**
     * The type of the data object. Used as the routing field when working with
     * a partitioned space.
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * The raw data this object holds.
     */
    public String getRawMessage() {
        return rawMessage;
    }

    /**
     * The raw data this object holds.
     */
    public void setRawMessage(String rawMessage) {
        this.rawMessage = rawMessage;
    }

    /**
     * The processed data this object holds.
     */
    public T getMessage() {
        return message;
    }

    /**
     * The processed data this object holds.
     */
    public void setMessage(T message) {
        this.message = message;
    }
    
    public void setFailure(Throwable ex) {
    	this.failure = ex;
    }
    
    public Throwable getFailure() {
    	return failure;
    }


    public String toString() {
    	return state.toString() + ": " + rawMessage;
    }
}
