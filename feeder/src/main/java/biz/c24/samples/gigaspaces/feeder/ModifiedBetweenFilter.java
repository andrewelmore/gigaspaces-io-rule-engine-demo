package biz.c24.samples.gigaspaces.feeder;

import java.io.File;
import java.io.FileFilter;

public class ModifiedBetweenFilter implements FileFilter {

	private long sinceTime = 0;
	private long untilTime = 0;
	
	public ModifiedBetweenFilter(long sinceTimeMs, long untilTimeMs) {
		sinceTime = sinceTimeMs > 0? sinceTimeMs : 0;
		untilTime = untilTimeMs > 0? untilTimeMs : 0;
		
	}

	@Override
	public boolean accept(File pathname) {
		return pathname.lastModified() > sinceTime && pathname.lastModified() <= untilTime;
	}
}
