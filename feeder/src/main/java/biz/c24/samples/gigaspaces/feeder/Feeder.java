package biz.c24.samples.gigaspaces.feeder;

import biz.c24.samples.gigaspaces.common.Message;

import org.openspaces.core.GigaSpace;
import org.openspaces.core.context.GigaSpaceContext;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * A feeder bean starts a scheduled task that writes a new Data objects to the space
 * (in an unprocessed state).
 *
 * <p>The space is injected into this bean using OpenSpaces support for @GigaSpaceContext
 * annotation.
 *
 * <p>The scheduling uses the java.util.concurrent Scheduled Executor Service. It
 * is started and stopped based on Spring life cycle events.
 *
 * @author kimchy
 */
public class Feeder implements InitializingBean, DisposableBean {

    Logger log= Logger.getLogger(this.getClass().getName());
    
    private ScheduledExecutorService executorService;

    private ScheduledFuture<?> sf;

    private long numberOfTypes = Message.State.values().length;

    private long defaultDelay = 10;

    private FeederTask feederTask;

    @GigaSpaceContext
    private GigaSpace gigaSpace;

    /**
     * Sets the number of types that will be used to set {@link org.openspaces.Message.data.common.Data#setType(Long)}.
     *
     * <p>The type is used as the routing index for partitioned space. This will affect the distribution of Data
     * objects over a partitioned space.
     */
    public void setNumberOfTypes(long numberOfTypes) {
        this.numberOfTypes = numberOfTypes;
    }

    public void setDefaultDelay(long defaultDelay) {
        this.defaultDelay = defaultDelay;
    }

    public void afterPropertiesSet() throws Exception {
        log.info("--- STARTING FEEDER WITH CYCLE [" + defaultDelay + "]");
        executorService = Executors.newScheduledThreadPool(1);
        feederTask = new FeederTask("/tmp/mt103");
        sf = executorService.scheduleAtFixedRate(feederTask, defaultDelay, defaultDelay,
                TimeUnit.MILLISECONDS);
    }

    public void destroy() throws Exception {
        sf.cancel(false);
        sf = null;
        executorService.shutdown();
    }
    
    public long getFeedCount() {
        return feederTask.getFeedCount();
    }
    
    
    public class FeederTask implements Runnable {
    	
    	private File dir;
    	private long lastCheckMs = 0;
    	private long count = 0;
    	
    	public FeederTask(String path) {
    		this.dir = new File(path);
    	}
    	
        public void run()  {
        	// Process all files which have been modified since we last checked
        	// -1 to ensure that we don't miss any files that are created after we scan but in the same millisecond
        	long untilTime = System.currentTimeMillis() - 1;
            File[] files = dir.listFiles(new ModifiedBetweenFilter(lastCheckMs, untilTime));
            lastCheckMs = untilTime;
            for(File file : files) {
            	if(file.isFile() && file.getName().endsWith(".dat")) {
					try {
						Scanner scanner = new Scanner(file);
                        String fileContent = scanner.useDelimiter("\\A").next();
                        scanner.close();
                        gigaSpace.write(new Message(fileContent));
                        count++;
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}            		
            	}
            }       
        }
        
        public long getFeedCount() {
        	return count;
        }
    }
}
